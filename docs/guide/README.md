<!--
 * @Author: 一尾流莺
 * @Description:
 * @Date: 2021-09-13 16:45:44
 * @LastEditTime: 2021-09-14 10:22:37
 * @FilePath: \warblerjs-guide\docs\guide\README.md
-->

## 安装

### 通过 npm

```js
npm i --save warbler-js
```

## 什么是 warbler-js

`warbler-js` 是一个 `JavaScript` 实用工具库。

