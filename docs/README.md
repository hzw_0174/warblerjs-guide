---
home: true
heroImage: /images/main.jpg
heroText: warbler-js
tagline: 一个 JavaScript 实用工具库
actionText: 点击阅读 →
actionLink: /guide/
features:
  - title: 按需引入
    details: 用到哪个，引入那个，不会白白增加代码体积。
  - title: 无需引入
    details: 源码简易，清晰，带有示例，直接复制文档即可使用。
  - title: 学无止境
    details: 百尺竿头须进步，十方世界是全身。
footer: MIT Licensed | Copyright © 2021 一尾流莺
---
